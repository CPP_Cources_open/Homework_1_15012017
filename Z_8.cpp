/*
    
    /-------\  /-------\  /       \  /-------\  /-------\  /-------\  /
    |          |       |  |       |  |              |      /       \  |
    |          |       |  |       |  |              |      /       \  |
    |          \-------/  \-------/  \-------\      |      /-------\  |
    |          |  \               |          |      |      /       \  |
    |          |     \            |          |      |      /       \  |
    \-------/  |       \  \-------/  \-------/      |      /       \  \-------/
    
    /-------\  /-------\  /       \  |-----\    ---------  /-------\
    |              |      |       |  |      \       |      |       |
    |              |      |       |  |       \      |      |       |
    \-------\      |      |       |  |       |      |      |       |
            |      |      |       |  |       /      |      |       |
            |      |      |       |  |      /       |      |       |
    \-------/      |      \-------/  |-----/    ---------  \-------/
        
*/

#include<iostream>
#define days_in_month 30 //I assigned 30 days in month because we don't know what months we should count
#define days_in_year 365 //And 365 days per year
#define year_profit 0.05

using namespace std;

int main()
{
    setlocale(0, "rus");
    float deposite;
    unsigned short int month_count;
    cout << "������� ����� �������� � �������� ���: ";
    cin >> deposite;
    cout << "������� ���������� ������� �������� ����� � �����: ";
    cin >> month_count;
    float profit = deposite * year_profit / days_in_year * days_in_month;
    cout << "������� � �������� � �����: " << profit << " �������� ���" << endl <<
            "������� � �������� �� ���� ������: " << profit * month_count << " �������� ���" << endl <<
            "����� � ������� �� ��������� ������: " << deposite + profit * month_count << "�������� ���";
    return 0;
}
